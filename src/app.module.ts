import { Module, Injectable, Inject, DynamicModule, forwardRef, Controller, Catch } from '@nestjs/common';

// A provider/interceptor/guard (or even an injectable exception filter) that has nothing:
@Injectable()
class NoopProvider {}

class EmptyProvider {}

// A traditional service:
@Injectable()
class MyService {
  constructor(private readonly _noopProvider: NoopProvider) {}
}

// A valid provider;
class MyServiceWithoutInjectable {
  constructor(@Inject('foo') private readonly foo: any) {}
}

// A traditional module that wrongly imports an injectable:
@Module({
  imports: [NoopProvider],
})
class NoopModule {}

// A module that was wrongly(?) used as an injectable as well:
@Module({})
@Injectable()
class NoopInjectableModule {}

// A traditional module that doesn't imports an injectable and uses injection:
@Module({
  providers: [ { provide: 'bar', useValue: 456 } ]
})
class ModuleWithInjection {
  constructor(@Inject('bar') private readonly _bar: any) {}
}

// A dynamic NestJS module that wrongly imports an injectable:
const dynamicModule: DynamicModule = {
  module: class Foo{},
  imports: [NoopProvider],
}

@Controller()
class NoopController {}

@Catch()
class NoopExceptionFilter {}

@Catch()
@Injectable()
class NoopInjectableExceptionFilter {}


@Module({
  /// Notice that `NoopProvider` doesn't need to be listed below to trigger
  /// those warnings. It only need to appear in any `imports` array of some
  /// loaded module.
  providers: [EmptyProvider, NoopProvider, MyService, MyServiceWithoutInjectable, { provide: 'foo', useValue: 123 }],
  imports: [
    /// This doesn't triggers the warning, as usual
    ModuleWithInjection,

    /// This triggers the warning due to the `@Injectable()` even tho it's a valid module
    NoopInjectableModule,

    /// This doesn't triggers the warning because this provider doesn't have
    /// the `@Injectable()` decorator 
    EmptyProvider,

    /// This triggers the warning due to the import of an injectable
    NoopModule,

    /// This doesn't triggers the warning because it doesn't have `@Injectable()`
    /// and still throws UnknownDependenciesException
    MyServiceWithoutInjectable,

    /// This triggers the warning due to the `@Controller()`
    NoopController,

    /// This triggers the warning due to the `@Catch()`
    NoopExceptionFilter,

    /// This triggers the warning due to the `@Injectable()`
    NoopInjectableExceptionFilter,

    /// This triggers the warning due to the `@Injectable()`
    NoopProvider,

    /// This triggers the warning (but only if it wasn't triggered already) due
    /// to the wrong import of an injectable
    dynamicModule,

    /// This triggers the warning (for the second time if it was triggered already).
    /// due to the `@Injectable()`
    forwardRef(() => NoopProvider),

    /// This triggers the warning (for the second time if it was triggered already)
    /// due to the `@Injectable()`, and still throws UnknownDependenciesException,
    /// as it has some dependency to be injected.
    MyService,
  ],
})
export class AppModule {}


/// A module that wrongly imports an injectable but is not registered,
/// thus it doesn't triggers the warning
@Module({
  imports: [NoopProvider],
})
class UnregisteredModule {}
